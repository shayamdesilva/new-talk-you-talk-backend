package com.utee.springbootstarter.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.utee.springbootstarter.common.JwtToken;
import com.utee.springbootstarter.domain.EmailRequestDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.services.EmailService;

@RestController
@RequestMapping("api/email")
public class EmailController {
	
	@Autowired
	EmailService emailService;
	
	@Autowired
	HttpServletRequest request;
	
	
	@PostMapping("/sendmail")
	public EmailRequestDTO sendEmail(@RequestBody UserDTO userDTO){
		JwtToken jwtToken = new JwtToken();
		String email = userDTO.getEmail();
		String token = jwtToken.generateJWTToken(userDTO);
		return emailService.sendEmail(email,token,76);
	}
	
	@GetMapping("/sendEmail/{email}")
	public UserValidationDTO verifyEmail(@PathVariable(value = "email",required = true) String email)
	{
		String value = email;
		return emailService.verifyEmailConformation(email);
	}
	
	@PostMapping("/requestTokenForResetPassword")
	public ResponseEntity<UserValidationDTO> requestTokenForResetPassword(@RequestBody Map<String,Object> userMap){
		String email = (String)userMap.get("email");
		UserValidationDTO userDto = emailService.requestTokenForResetPasword(email);
		 return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
		
	}
	
	@PostMapping("/resetpassword")
	public ResponseEntity<UserValidationDTO> resetPassword(@RequestBody Map<String,Object> userMap){
		UserDTO dto = new UserDTO();
		String email = userMap.get("email").toString();
		String passWord  = userMap.get("usrPasswrd").toString();
		String token = userMap.get("token").toString();
				
		UserValidationDTO userDto = emailService.resetPassword(email, passWord, token);
		 return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
		
	}
	
	
	

}
