package com.utee.springbootstarter.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.utee.springbootstarter.common.Utils;
import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.services.RegisterUserServices;

@RestController
@RequestMapping("/api/profile")
public class ProfileController {
	
	@Autowired
	private RegisterUserServices registerUserServices;

	@GetMapping("")
	public String getAllCatagories(HttpServletRequest httpServletRequest){
		int userId = (Integer) httpServletRequest.getAttribute("userId");
		return "Authenticated! USERID "+userId;
	}
	
	@PostMapping("/setupprofile")
	public ResponseEntity<Map<String, String>>  fileUpload(@RequestParam("file") MultipartFile file, @RequestParam Integer userId,@RequestParam String userName) {

		UserDTO dto = new UserDTO();
		dto.setUserId(userId);
		dto.setUserName(userName);
		UserDTO userDto = registerUserServices.updateUser(file,dto);
		Utils utils = new Utils();
//		String path = utils.getCloudinaryUrlByMultipartImage(file);
//		System.out.println("Image Path is : "+path);

		Map<String,String> map = new HashMap<>();
		map.put("message", "File Uploaded sucessfully");
		  return new ResponseEntity<Map<String,String>>(map,HttpStatus.OK);

		
//		return new ModelAndView("status", "message", "File Uploaded sucessfully");
	}
	
	
	@PostMapping("/searchusername")
	public ResponseEntity<UserValidationDTO> searchUserByUserName(@RequestBody Map<String,Object> userMap){
		String userName = (String)userMap.get("userName");
		UserValidationDTO userDto = registerUserServices.getUserByUserName(userName);
		
		Map<String,String> map = new HashMap<>();
		map.put("result", userDto.toString() );
		  return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
	}
	
	@PostMapping("/searchfullname")
	public ResponseEntity<UserValidationDTO> searchUserByFullName(@RequestBody Map<String,Object> userMap){
		String firstName = (String)userMap.get("firstName");
		UserValidationDTO userDto = registerUserServices.getUserByUserName(firstName);
		
		Map<String,String> map = new HashMap<>();
		map.put("result", userDto.toString() );
		  return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
	}
	
	
	@PostMapping("/followuser")
	public ResponseEntity<UserValidationDTO> followUser(@RequestBody FollowUserDTO followUserDTO){
			registerUserServices.followUser(followUserDTO);
			return null;
	}

}
