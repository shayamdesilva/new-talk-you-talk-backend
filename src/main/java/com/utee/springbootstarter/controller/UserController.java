package com.utee.springbootstarter.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.utee.springbootstarter.common.Constant;
import com.utee.springbootstarter.common.Utils;
import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.FollowUserDetailViewDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.services.RegisterUserServices;
import com.utee.springbootstarter.services.UserService;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@RestController
@RequestMapping("api/users")
public class UserController {

	@Autowired
	private RegisterUserServices registerUserServices;
	
	@Autowired
	private HttpServletRequest context;
	
	@Autowired
	private UserService userService;
	
	
	private static String UPLOAD_FOLDER = "C://test//";
	
	@GetMapping("/test")
	public String test(){
		return "Hello TEST....!!!!";
	}
	
	@GetMapping("/userbyemail")
	public UserDTO getUserByEmail(@RequestBody UserDTO dto){
		return registerUserServices.loginUser(dto.getEmail(),dto.getUsrPasswrd());
	}
	
	
	@PostMapping("/login")
	public ResponseEntity<UserValidationDTO> loginUser(@RequestBody Map<String,Object> userMap){
		String email = (String)userMap.get("email");
		String password = (String)userMap.get("usrPasswrd");
		UserValidationDTO userDto = registerUserServices.loginUserDto(email,password);
		Map<String,String> map = new HashMap<>();
		map.put("result", userDto.toString() );
		  return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
		
	}
	
	@PostMapping("/register")
	public ResponseEntity<UserValidationDTO> addUser(@RequestBody UserDTO dto){
		UserValidationDTO userDto = registerUserServices.registerUserDto(dto);
		Map<String,String> map = new HashMap<>();
		map.put("result", userDto.toString() );
		  return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
	}
	
	
	@PostMapping("/userlist")
	public ResponseEntity<UserValidationDTO> listUsers(){
		UserValidationDTO userDto = registerUserServices.userList();
		
		Map<String,String> map = new HashMap<>();
		map.put("result", userDto.toString() );
		  return new ResponseEntity<UserValidationDTO>(userDto,HttpStatus.OK);
	}
	

	@PostMapping("/getallfollowuserbyid") //
	public ResponseEntity<UserValidationDTO> getUserDetailByFollower(@RequestBody FollowUserDetailViewDTO dto){ 
		UserValidationDTO x = userService.getFollowUsersById(dto.getUserId());
		return new ResponseEntity<UserValidationDTO>(x,HttpStatus.OK);
	}
	
	
	@PostMapping("/getallfollowactorbyid")//
	public ResponseEntity<UserValidationDTO> getFollowActor(@RequestBody FollowUserDetailViewDTO dto){ 
		UserValidationDTO x = userService.getFollowUsersById(dto.getActor_id());
		return new ResponseEntity<UserValidationDTO>(x,HttpStatus.OK);
	}
	

	

	
	
	
//	@PostMapping("/saveuser")
//	public ResponseEntity<Map<String,String>> getUserByEmail(@RequestBody UserDTO dto){
//		UserDTO userDTO = UserServices.registerUser(dto);
//		Map<String,String> map = new HashMap<>();
//		map.put("message", "Registed Sucessfull");
//		return new ResponseEntity<>(map,HttpStatus.OK);
//	}
	
}
