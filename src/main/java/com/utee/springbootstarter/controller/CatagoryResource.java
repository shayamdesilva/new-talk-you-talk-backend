package com.utee.springbootstarter.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/categories")
public class CatagoryResource {
	
	@GetMapping("")
	public String getAllCatagories(HttpServletRequest httpServletRequest){
		int userId = (Integer) httpServletRequest.getAttribute("userId");
		return "Authenticated! USERID "+userId;
	}

}
