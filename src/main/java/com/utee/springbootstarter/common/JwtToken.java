package com.utee.springbootstarter.common;

import java.util.Date;

import com.utee.springbootstarter.domain.UserDTO;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class JwtToken {
	
	public String generateJWTToken(UserDTO dto){
		Long timeStamp = System.currentTimeMillis();
		String token = Jwts.builder().signWith(SignatureAlgorithm.HS256, Constant.API_SECRET_KEY)
				.setIssuedAt(new Date(timeStamp))
				.setExpiration(new Date(timeStamp + Constant.TAKEN_VALIDITY))
				.claim("QrCode", dto.getQrCode())
				.claim("email", dto.getEmail())
				.claim("fullName", dto.getFirstName())
				.claim("userId", dto.getUserId())
				.compact();
		return token;
	}
	

}
