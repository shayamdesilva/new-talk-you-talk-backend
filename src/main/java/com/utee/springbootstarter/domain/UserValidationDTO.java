package com.utee.springbootstarter.domain;

import java.util.List;

public class UserValidationDTO {
	private String accessToken;
	
	private List<UserDTO> userDto;
	
	public List<UserDTO> getUserDto() {
		return userDto;
	}
	public void setUserDto(List<UserDTO> userDto) {
		this.userDto = userDto;
	}
	private int status;
	
	private int httpCode;
	
	private List<ValidationFactorsDTO> validationFactor;
	
	private List<FollowUserDetailViewDTO> followUser;
	
	public List<FollowUserDetailViewDTO> getFollowUser() {
		return followUser;
	}
	public void setFollowUser(List<FollowUserDetailViewDTO> followUser) {
		this.followUser = followUser;
	}
	private ErrorDTO error;
	
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public int getHttpCode() {
		return httpCode;
	}
	public void setHttpCode(int httpCode) {
		this.httpCode = httpCode;
	}
	public List<ValidationFactorsDTO> getValidationFactor() {
		return validationFactor;
	}
	public void setValidationFactor(List<ValidationFactorsDTO> validationFactor) {
		this.validationFactor = validationFactor;
	}
	public ErrorDTO getError() {
		return error;
	}
	public void setError(ErrorDTO error) {
		this.error = error;
	}
	
	

}


