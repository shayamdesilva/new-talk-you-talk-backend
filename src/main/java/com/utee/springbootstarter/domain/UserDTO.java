package com.utee.springbootstarter.domain;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="utee20_tut_user")
public class UserDTO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer userId;

	@Column(name="system_key")
	private String systemKey;
	
	@Column(name="username")
	private String userName;
	
	@Column(name="firstName")
	private String firstName;
	
	@Column(name="lastName")
	private String lastName;
	
	@Column(name="email")
	private String email;
	
	@Column(name="mobile")
	private String mobile;
	
	@Column(name="usr_passwrd")
	private String usrPasswrd;
	
	@Column(name="usr_reset_passwrd")
	private String usrResetPasswrd;
	
	@Column(name="profile_img_url")
	private String profileImgUrl;
	
	@Column(name="is_email_verified")
	private Boolean isEmailVerified;
	
	@Column(name="is_mobile_verified")
	private Boolean isMobileVerified;
	
	@Column(name="qr_code")
	private String qrCode;
	
	@Column(name="registered_date")
	private Date registeredDate;
	
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) 
	@OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "user_email_id")
	private UserEmailDTO userEmailDTO;
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getSystemKey() {
		return systemKey;
	}

	public void setSystemKey(String systemKey) {
		this.systemKey = systemKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getUsrPasswrd() {
		return usrPasswrd;
	}

	public void setUsrPasswrd(String usrPasswrd) {
		this.usrPasswrd = usrPasswrd;
	}

	public String getProfileImgUrl() {
		return profileImgUrl;
	}

	public void setProfileImgUrl(String profileImgUrl) {
		this.profileImgUrl = profileImgUrl;
	}

	public Boolean getIsEmailVerified() {
		return isEmailVerified;
	}

	public void setIsEmailVerified(Boolean isEmailVerified) {
		this.isEmailVerified = isEmailVerified;
	}

	public Boolean getIsMobileVerified() {
		return isMobileVerified;
	}

	public void setIsMobileVerified(Boolean isMobileVerified) {
		this.isMobileVerified = isMobileVerified;
	}

	public String getQrCode() {
		return qrCode;
	}

	public void setQrCode(String qrCode) {
		this.qrCode = qrCode;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public String getUsrResetPasswrd() {
		return usrResetPasswrd;
	}

	public void setUsrResetPasswrd(String usrResetPasswrd) {
		this.usrResetPasswrd = usrResetPasswrd;
	}

	public UserEmailDTO getUserEmailDTO() {
		return userEmailDTO;
	}

	public void setUserEmailDTO(UserEmailDTO userEmailDTO) {
		this.userEmailDTO = userEmailDTO;
	}
}
