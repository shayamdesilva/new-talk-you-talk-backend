package com.utee.springbootstarter.services;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Content;
import com.sendgrid.helpers.mail.objects.Email;
import com.sendgrid.helpers.mail.objects.GoogleAnalyticsSetting;
import com.sendgrid.helpers.mail.objects.Personalization;
import com.sendgrid.helpers.mail.objects.SubscriptionTrackingSetting;
import com.sendgrid.helpers.mail.objects.TrackingSettings;
import com.twilio.rest.chat.v1.service.User;
import com.utee.springbootstarter.common.Constant;
import com.utee.springbootstarter.common.JwtToken;
import com.utee.springbootstarter.common.Utils;
import com.utee.springbootstarter.domain.EmailRequestDTO;
import com.utee.springbootstarter.domain.ErrorDTO;
import com.utee.springbootstarter.domain.ResetPassword;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserEmailDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.domain.ValidationFactorsDTO;
import com.utee.springbootstarter.repositories.EmailRepository;
import com.utee.springbootstarter.repositories.RegisterUserRepository;
import com.utee.springbootstarter.repositories.ResetPasswordRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

@Service
public class EmailServiceImpl implements EmailService {

	@Value("${app.sendgrid.templateId}")
	private String templateId;
	
	@Value("${app.sendgrid.key}")
	private String SENDGRID_API_KEY;
	
	@Value("${sengrid.email}")
	private String sengridEmail;
	
	@Value("${sengrid.emailSubject}")
	private String sengridEmailSubject;
	
	@Value("${sengrid.emailconformurl}")
	private String sengridEmailConformUrl;
	
	@Autowired
	EmailRepository emailRepo;
	
	@Autowired
	ResetPasswordRepository resetPasswordRepository; 
	
	@Autowired
	RegisterUserRepository registerUserReop;

	
	@Autowired
	SendGrid sendGrid;
	
	@Override
	public String sendEmail1(String email) {
		TrackingSettings trackingSettings = new TrackingSettings();
		Mail mail = new Mail();		
		SubscriptionTrackingSetting subscriptionTrackingSetting = new SubscriptionTrackingSetting();
		    subscriptionTrackingSetting.setEnable(true);
		    subscriptionTrackingSetting.setText("text to insert into the text/plain portion of the message");
		    subscriptionTrackingSetting.setHtml("<html><body>html to insert into the text/html portion of the message</body></html>");
		    subscriptionTrackingSetting.setSubstitutionTag("Optional tag to replace with the open image in the body of the message");
		    trackingSettings.setSubscriptionTrackingSetting(subscriptionTrackingSetting);
		    GoogleAnalyticsSetting googleAnalyticsSetting = new GoogleAnalyticsSetting();
		    googleAnalyticsSetting.setEnable(true);
		    googleAnalyticsSetting.setCampaignSource("some source");
		    googleAnalyticsSetting.setCampaignTerm("some term");
		    googleAnalyticsSetting.setCampaignContent("some content");
		    googleAnalyticsSetting.setCampaignName("some name");
		    googleAnalyticsSetting.setCampaignMedium("some medium");
		    trackingSettings.setGoogleAnalyticsSetting(googleAnalyticsSetting);
			mail = new Mail(new Email(sengridEmail), sengridEmailSubject, new Email(email),new Content("text/html", "<h1>Hello</h1>"));		
			mail.setTrackingSettings(trackingSettings);
			
		
		    
			mail.setReplyTo(new Email(sengridEmail));
			Request request = new Request();

			Response response = null;

			try {

				request.setMethod(Method.POST);

				request.setEndpoint("mail/send");

				request.setBody(mail.build());

				response=this.sendGrid.api(request);
				
				System.out.println("Response is : "+response.getStatusCode());
				System.out.println("Response is : "+response.getBody());

			} catch (Exception ex) {


			}
			return "Email Send Sucess";
		
	}
	
	@Override
	public EmailRequestDTO sendEmail(String email,String token,Integer userId) {
		EmailRequestDTO emailDto = new EmailRequestDTO();
		
		UserEmailDTO userEmailDTO =  emailRepo.findEmailVerificationKeyByUserId(userId, email);
		if(userEmailDTO == null || (userEmailDTO != null && !userEmailDTO.isEmailVerify()) ){
			
			// message contains HTML markups
	        String message = "<i>Greetings!</i><br>";
	        message += "<h2>Let's verify your single sender so you can start sending email. </h2><br>";
	        message += "<a href="+sengridEmailConformUrl+token+">"+ "<h3> Email Conform</h3>" +" </a>";
			
			Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
			
			Mail mail = new Mail(new Email(sengridEmail), sengridEmailSubject, new Email(email),new Content("text/html", message));
			mail.setReplyTo(new Email(sengridEmail));
			Request request = new Request();

			Response response = null;

			try {

				request.setMethod(Method.POST);

				request.setEndpoint("mail/send");

				request.setBody(mail.build());

				response=this.sendGrid.api(request);
				if(response.getStatusCode() == 200 || response.getStatusCode() == 202){
					JwtToken jwtToken = new JwtToken();
//					String email = userDTO.getEmail();
					UserDTO userDTO = new UserDTO();
					userDTO.setEmail(email);
					userDTO.setUserId(userId);
					String jtToken = jwtToken.generateJWTToken(userDTO);
					
					UserEmailDTO emailDTO = new UserEmailDTO();
					emailDTO.setEmail(email);
					emailDTO.setEmailKey(token);
					emailDTO.setCreateDate(new Date());
					emailDTO.setUserId(userId);
					emailDto.setStatusCode(200);
					emailDto.setStatus(Constant.EMAILSUCESS);
					emailDto.setUserEmailDTO(emailDTO);
					return emailDto;
					
					}else{
						
						emailDto.setStatusCode(response.getStatusCode());
						emailDto.setStatus(response.getBody());
						return emailDto;
					}
				

			} catch (IOException ex) {

				System.out.println(ex.getMessage());

			}
			
		}
		
		
		
		return emailDto;
		
	}
	
	

	@Override
	public UserValidationDTO validateUserConformation(String token) {
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		UserDTO dto = new UserDTO();
		List<UserDTO> listDto = new ArrayList<>();
		ErrorDTO errorDTO = new ErrorDTO();
		UserEmailDTO userEmailDTO = emailRepo.findEmailVerificationDtoByEmailKey(token);
		if(userEmailDTO != null && !userEmailDTO.isEmailVerify()){
			emailRepo.updateUserEmailByEmaiID(userEmailDTO.getEmailId(), true, new Date());
			registerUserReop.updateUserEmailVerificationByUserid(userEmailDTO.getUserId(), true);
			
			userValidationDTO.setHttpCode(200);
			userValidationDTO.setStatus(1);
			userValidationDTO.setError(errorDTO);
			return userValidationDTO;
			
		}if(userEmailDTO != null && userEmailDTO.isEmailVerify()){
			errorDTO.setMessage("Email Already Confirm ");
			errorDTO.setError("You have already clicked confirm link ");
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setStatus(0);
			userValidationDTO.setError(errorDTO);
			return userValidationDTO;
			
		}else{
			
			errorDTO.setMessage("Not such email account ");
			errorDTO.setError("Email confirm link is invalid please try again ");
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setStatus(0);
			userValidationDTO.setError(errorDTO);
			return userValidationDTO;
			
		}
	}

	@Override
	public UserValidationDTO verifyEmailConformation(String token) {
		UserValidationDTO userValidationDTO = validateUserConformation(token);
		
		return userValidationDTO;
	}

	@Override
	public UserValidationDTO requestTokenForResetPasword(String email) {
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		UserDTO userDTO = registerUserReop.findByEmail(email);
		Utils utils = new Utils();
		if(userDTO != null){
			
			ResetPassword  resetPasswordDto = resetPasswordRepository.findByEmail(email);
			String token = String.valueOf(utils.generateToken());
//			ResetPassword resetPassword = new ResetPassword();
			
			if(resetPasswordDto != null){
				resetPasswordDto.setCreateDate(new Date());
				resetPasswordDto.setToken( token  );
				resetPasswordRepository.save(resetPasswordDto);
			}else{
				ResetPassword resetPassword = new ResetPassword();
				resetPassword.setCreateDate(new Date());
				resetPassword.setEmail(userDTO.getEmail());
				resetPassword.setUserId(userDTO.getUserId());
				resetPassword.setToken( token  );
				resetPasswordRepository.save(resetPassword);
			}
			EmailRequestDTO emailRequestDTO = sendEmailWithToken(email, token);
			userValidationDTO.setHttpCode(200);
			userValidationDTO.setStatus(1);
			return userValidationDTO;
		}else{
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setStatus(0);
			return userValidationDTO;
		}
	}

	@Override
	public boolean validateEmailAddress(String email) {
		UserEmailDTO emailDTO = emailRepo.isValidEmailByEmailAddress(email);
		if(emailDTO != null){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public EmailRequestDTO sendEmailWithToken(String email,String token) {
EmailRequestDTO emailDto = new EmailRequestDTO();
		
		UserDTO userDTO = registerUserReop.findByEmail(email);
		if(userDTO != null || (userDTO != null && !userDTO.getIsEmailVerified()) ){
			
			 String message = "<h1>Talk You Talk</h1>";
		        message += "<h2>We received a request to reset your password for your Talk You Talk account: </h2><br>";
		        message += email + "<h3>  We're here to help!</h3><br /> " ;
		        message += "<h3>Simply click on the button to set a new password:</h3> <br />";
		        message += "<h1>"+token + "</h1> <br />";
		        message +="<h3>If you didn't ask to change your password, don't worry! Your password is still safe and you can delete this email.</h3> </br /><br />";
		        message +="<h3> Cheers,</h3>";
				
				Content content = new Content("text/plain", "and easy to do anywhere, even with Java");
				
				Mail mail = new Mail(new Email(sengridEmail), Constant.FORGTEMAILTOKENHEADER, new Email(email),new Content("text/html", message));
				mail.setReplyTo(new Email(sengridEmail));
				Request request = new Request();

				Response response = null;

				try {

					request.setMethod(Method.POST);

					request.setEndpoint("mail/send");

					request.setBody(mail.build());

					response=this.sendGrid.api(request);
				if(response.getStatusCode() == 200 || response.getStatusCode() == 202){
					Utils utils = new Utils();
					userDTO.setUsrResetPasswrd(utils.encrypyPassword(token));
					registerUserReop.save(userDTO);
					emailDto.setStatus("Email send");
					emailDto.setStatusCode(response.getStatusCode());
					return emailDto;
					
					}else{
						
						emailDto.setStatusCode(response.getStatusCode());
						emailDto.setStatus(response.getBody());
						return emailDto;
					}
				

			} catch (IOException ex) {

				System.out.println(ex.getMessage());

			}
			
		}
		
		return emailDto;
	}

	@Override
	public UserValidationDTO resetPassword(String email,String newPassword,String token) {
		
		ResetPassword resetPassword = resetPasswordRepository.findByEmail(email);
		UserDTO userDTO = registerUserReop.findByEmail(email);
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		Utils utils = new Utils();
		ErrorDTO errorDTO = new ErrorDTO();
		if(resetPassword != null){
			Long createDate = resetPassword.getCreateDate().getTime();
			Long afterDate =  Constant.RESETPASSWORDVALIDTIME +createDate;
			Long newDate = new Date().getTime();
			
			if(!resetPassword.getToken().equals(token)){
				errorDTO.setError("Invalid Token");
				errorDTO.setMessage("Toke is incorrect");
				userValidationDTO.setHttpCode(500);
				userValidationDTO.setStatus(0);
				userValidationDTO.setError(errorDTO);
				return userValidationDTO;
				
			}
			
			if(afterDate > newDate){
				List<UserDTO> userDtoList = new ArrayList<>();
				userDTO.setUsrPasswrd(utils.encrypyPassword(newPassword));
				userDtoList.add(userDTO);
				registerUserReop.save(userDTO);
				userValidationDTO.setHttpCode(200);
				userValidationDTO.setStatus(1);
				userValidationDTO.setUserDto(userDtoList);
				return userValidationDTO;
				
			}else{
				errorDTO.setError("Time Expire");
				errorDTO.setMessage("Please Enter code befor");
				userValidationDTO.setHttpCode(500);
				userValidationDTO.setStatus(0);
				userValidationDTO.setError(errorDTO);
				return userValidationDTO;
				
			}
		}else{
			errorDTO.setError("Invalid Email");
			errorDTO.setMessage("Email not found");
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setStatus(0);
			userValidationDTO.setError(errorDTO);
			return userValidationDTO;
			
		}
		
//		UserDTO userDTO = registerUserReop.findByEmail(email);
//		UserValidationDTO userValidationDTO = new UserValidationDTO();
//		List<ValidationFactorsDTO> userDtoList = new ArrayList<ValidationFactorsDTO>();
//		List<UserDTO> userDTOs = new ArrayList<>();
//		Utils utils = new Utils();
//		
//		if(!BCrypt.checkpw(dto.getUsrResetPasswrd(),userDTO.getUsrResetPasswrd())){
//			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
//			factorsDTO.setFieldName(Constant.PASSWORD);
//			factorsDTO.setValidationError(Constant.LOGINFAIL);
//			userDtoList.add(factorsDTO);
////			userValidationDTO.setValidationFactor(validateUserDtoList);
//			userValidationDTO.setStatus(0);
//			userValidationDTO.setHttpCode(500);
//			userValidationDTO.setValidationFactor(userDtoList);
//			return userValidationDTO;
//		}else{
//			userDTO.setUsrPasswrd(utils.encrypyPassword(dto.getUsrPasswrd()));
//			userDTOs.add(userDTO);
//			registerUserReop.save(userDTO);
//			userValidationDTO.setStatus(1);
//			userValidationDTO.setHttpCode(200);
//			userValidationDTO.setUserDto(userDTOs);
//			return userValidationDTO;
//		}
	}
	
	
	
	


}
