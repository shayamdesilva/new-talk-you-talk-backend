package com.utee.springbootstarter.services;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.FollowUserDetailViewDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.repositories.FollowUserDetailsRepository;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	FollowUserDetailsRepository followUserDetailsRepo;

//	@Override
//	public UserValidationDTO getUserDetailsByFollowUser(Integer id) {
//		UserValidationDTO  userValidationDTO = new UserValidationDTO();
//			List<FollowUserDetailViewDTO> listViewDto =  followUserDetailsRepo.findUserDetailById(id);
//			if(listViewDto.size() > 0){
//
//				 userValidationDTO.setFollowUser(listViewDto);
//				 userValidationDTO.setStatus(1);
//				 userValidationDTO.setHttpCode(200);
//				 return userValidationDTO;
//			}else{
//				 userValidationDTO.setStatus(0);
//				 userValidationDTO.setHttpCode(500);
//				 return userValidationDTO;
//			}
//	}

	@Override
	public UserValidationDTO getFollowUsersById(Integer id) {
		UserValidationDTO  userValidationDTO = new UserValidationDTO();
		List<FollowUserDetailViewDTO> listViewDto =  followUserDetailsRepo.findFollowUsersDetailById(id);
		if(listViewDto.size() > 0){

			 userValidationDTO.setFollowUser(listViewDto);
			 userValidationDTO.setStatus(1);
			 userValidationDTO.setHttpCode(200);
			 return userValidationDTO;
		}else{
			 userValidationDTO.setStatus(0);
			 userValidationDTO.setHttpCode(500);
			 return userValidationDTO;
		}
	}

	@Override
	public UserValidationDTO getFollowActorById(Integer id) {
		UserValidationDTO  userValidationDTO = new UserValidationDTO();
		List<FollowUserDetailViewDTO> listViewDto =  followUserDetailsRepo.findActorsDetailById(id);
		if(listViewDto.size() > 0){

			 userValidationDTO.setFollowUser(listViewDto);
			 userValidationDTO.setStatus(1);
			 userValidationDTO.setHttpCode(200);
			 return userValidationDTO;
		}else{
			 userValidationDTO.setStatus(0);
			 userValidationDTO.setHttpCode(500);
			 return userValidationDTO;
		}
	}

}
