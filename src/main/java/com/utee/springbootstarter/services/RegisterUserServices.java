package com.utee.springbootstarter.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.exceptions.EtAuthException;

public interface RegisterUserServices {
	
//	List<UserDTO> findAllUser();

	UserDTO registerUser(UserDTO dto) throws EtAuthException;
	
	UserDTO loginUser(UserDTO dto) throws EtAuthException;
	
	UserDTO loginUser(String email,String password) throws EtAuthException;
	
	UserDTO updateUser(MultipartFile multipartFile,UserDTO dto) throws EtAuthException;
	
	UserValidationDTO registerUserDto(UserDTO dto);
	
	UserValidationDTO loginUserDto(String email,String password);
	
	UserValidationDTO userList();
	
	UserValidationDTO getUserByUserName(String userName);
	
	UserValidationDTO getUserByFirstName(String firstName);
	
	UserValidationDTO followUser(FollowUserDTO dto);
	
	



}
