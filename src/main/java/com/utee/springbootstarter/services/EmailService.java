package com.utee.springbootstarter.services;

import com.sendgrid.helpers.mail.Mail;
import com.utee.springbootstarter.domain.EmailRequestDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;

public interface EmailService {
	
//	String sendEmail(String email,String token,Integer userId);
	
	EmailRequestDTO sendEmail(String email,String token,Integer userId);
	
	EmailRequestDTO sendEmailWithToken(String email,String token);
	
	String sendEmail1(String email);
	
	UserValidationDTO validateUserConformation(String token);
	
	UserValidationDTO verifyEmailConformation(String token);
	
	UserValidationDTO requestTokenForResetPasword(String email);
	
	UserValidationDTO resetPassword(String email,String newPassword,String token);
	
	boolean validateEmailAddress(String email);
	
//	Mail prepareEmail(String email);

}
