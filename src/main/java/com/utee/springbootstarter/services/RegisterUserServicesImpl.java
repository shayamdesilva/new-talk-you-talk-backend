package com.utee.springbootstarter.services;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Locale;
import java.util.regex.Pattern;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;

import java.util.Map;


import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.utee.springbootstarter.common.Constant;
import com.utee.springbootstarter.common.JwtToken;
import com.utee.springbootstarter.common.Utils;
import com.utee.springbootstarter.domain.EmailRequestDTO;
import com.utee.springbootstarter.domain.ErrorDTO;
import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.domain.ValidationFactorsDTO;
import com.utee.springbootstarter.exceptions.EtAuthException;
import com.utee.springbootstarter.repositories.FollowUserRepository;
import com.utee.springbootstarter.repositories.RegisterUserRepository;

import ch.qos.logback.classic.pattern.Util;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;

import org.springframework.web.multipart.MultipartFile;

import com.cloudinary.Transformation;

@Service
public class RegisterUserServicesImpl  implements RegisterUserServices{

	@Autowired
 	private  RegisterUserRepository userRepository;
	
	@Autowired
	private EmailService emailService;
	
	@Autowired
	private HttpServletRequest context;
	
	@Autowired
	private FollowUserRepository followUserRepository;
	
//	public List<UserDTO> findAllUser() {
//		List<UserDTO> userDto = userRepository.findAl
//		return userDto;
//	}
	
	public List<ValidationFactorsDTO> validateUserLoginDto(String email,String password){
		List<ValidationFactorsDTO> userDtoList = new ArrayList<ValidationFactorsDTO>();
		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
		if(email.equals(null) || email.isEmpty()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.USERNAME);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			 userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(password.equals(null) || password.isEmpty()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.PASSWORD);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			 userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(!pattern.matcher(email).matches()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.EMAIL);
			factorsDTO.setValidationError(Constant.INVALIDFORMAT);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(email != null) email = email.toLowerCase();
		UserDTO userDto =  userRepository.findByEmailAndPassword(email);
		if(userDto != null){
			if(!BCrypt.checkpw(password,userDto.getUsrPasswrd())){
				ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
				factorsDTO.setFieldName(Constant.PASSWORD);
				factorsDTO.setValidationError(Constant.LOGINFAIL);
				userDtoList.add(factorsDTO);
				return userDtoList;
			}
		}else{
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.EMAIL);
			factorsDTO.setValidationError(Constant.EMAILNOTFOUND);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		
		return userDtoList;
	}
	
	public List<ValidationFactorsDTO> validateUserDto(UserDTO userDto){
		List<ValidationFactorsDTO> userDtoList = new ArrayList<ValidationFactorsDTO>();
		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
		
		if(userDto.getFirstName().equals(null) || userDto.getFirstName().isEmpty()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.FIRSTNAME);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			 userDtoList.add(factorsDTO);
			 return userDtoList;

		}
		
		if(userDto.getFirstName().matches(".*\\d.*")){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.FIRSTNAME);
			factorsDTO.setValidationError(Constant.INVALIDFORMAT);
			 userDtoList.add(factorsDTO);
			 return userDtoList;

		}
		
		if(userDto.getLastName().equals(null) || userDto.getLastName().isEmpty()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.LASTNAME);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			 userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(userDto.getLastName().matches(".*\\d.*")){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.LASTNAME);
			factorsDTO.setValidationError(Constant.INVALIDFORMAT);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(userDto.getEmail().equals(null)){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.EMAIL);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(!pattern.matcher(userDto.getEmail()).matches()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.EMAIL);
			factorsDTO.setValidationError(Constant.INVALIDFORMAT);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		if(userDto.getUsrPasswrd().equals(null) || userDto.getUsrPasswrd().isEmpty()){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.PASSWORD);
			factorsDTO.setValidationError(Constant.EMPTYFIELD);
			userDtoList.add(factorsDTO);
			 return userDtoList;
		}
		
		
		
		return userDtoList;
	}

	
	public UserValidationDTO registerUserDto(UserDTO dto){
		Utils utils = new Utils();
		ErrorDTO errorDto = new ErrorDTO();
		JwtToken jwtToken = new JwtToken();
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		List<ValidationFactorsDTO> validateUserDtoList = validateUserDto(dto);
		if(validateUserDtoList.size()>0){
			userValidationDTO.setValidationFactor(validateUserDtoList);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			return userValidationDTO;
		}
		
		Integer count = userRepository.getCountByEmail(dto.getEmail());
		if(count > 0){
			errorDto.setError(Constant.EMAILDUPLICATE);
			errorDto.setMessage(Constant.EMAILDUPLICATE);
			userValidationDTO.setError(errorDto);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			return userValidationDTO;
		}
		
		String token = jwtToken.generateJWTToken(dto);
		String hashPassword = utils.encrypyPassword(dto.getUsrPasswrd());
//		String hashPassword = BCrypt.hashpw(dto.getUsrPasswrd(), BCrypt.gensalt(10));
		dto.setUsrPasswrd( hashPassword);
		List<UserDTO> userList = new ArrayList<UserDTO>();
		
		
		EmailRequestDTO emailRequestDTO =  emailService.sendEmail(dto.getEmail(), token, null);
		dto.setUserEmailDTO(emailRequestDTO.getUserEmailDTO());
		
		if(emailRequestDTO.getStatusCode() != 200){
			
			userValidationDTO.setValidationFactor(validateUserDtoList);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(emailRequestDTO.getStatusCode());
			errorDto.setError(emailRequestDTO.getStatus());
			errorDto.setMessage(emailRequestDTO.getStatus());
			userValidationDTO.setError(errorDto);
			return userValidationDTO;
		}
		UserDTO userDTO = userRepository.save(dto);
		userDTO.setUsrPasswrd("");
		userList.add(userDTO);
		userValidationDTO.setUserDto(userList);
		userValidationDTO.setHttpCode(200);
		userValidationDTO.setStatus(1);
		userValidationDTO.setAccessToken(token);
		
		


		return userValidationDTO;
	}
	
	public UserDTO registerUser(UserDTO dto) throws EtAuthException{
		
		Utils utils = new Utils();
		ErrorDTO errorDto = new ErrorDTO();
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		List<ValidationFactorsDTO> validateUserDtoList = validateUserDto(dto);
		if(validateUserDtoList.size()>1){
			userValidationDTO.setValidationFactor(validateUserDtoList);
			 
		}
		

//		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
//		UserDTO userDTO =dto;
//		if(dto.getEmail() != null) 
//			userDTO.setEmail(dto.getEmail().toLowerCase());
//		if(!pattern.matcher(dto.getEmail()).matches()){
//			throw new EtAuthException("Invalid Email Format");
//		}
		
		String hashPassword = BCrypt.hashpw(dto.getUsrPasswrd(), BCrypt.gensalt(10));
		Integer count = userRepository.getCountByEmail(dto.getEmail());
		if(count > 0){
			
			errorDto.setError(Constant.EMAILDUPLICATE);
			errorDto.setMessage(Constant.EMAILDUPLICATE);
		}

		
		
//		Pattern phonePattern = Pattern.compile("-?\\d+(\\.\\d+)?");
//		if(!phonePattern.matcher(dto.getMobile()).matches()){
//			throw new EtAuthException("Mobile Number is Invalid");
		
				
		dto.setUsrPasswrd( hashPassword);
		//userDTO.setProfileImgUrl(utils.getCloudinaryUrlByImage(dto.getProfileImgUrl()));
		return userRepository.save(dto);
	}

	public UserDTO loginUser(UserDTO dto) throws EtAuthException {
		return null;
	}

	@Override
	public UserDTO loginUser(String email, String password) throws EtAuthException {
		try {
			if(email != null) email = email.toLowerCase();
			UserDTO userDto =  userRepository.findByEmailAndPassword(email);
			if(!BCrypt.checkpw(password,userDto.getUsrPasswrd())){
				throw new EtAuthException("Invalid emai/password");
			}
			return userDto;
		} catch (Exception e) {
			throw new EtAuthException("Invalid emai/password");
		}
	}

	@Override
	public UserDTO updateUser(MultipartFile multipartFile, UserDTO dto) throws EtAuthException {
		try {
			Utils utils = new Utils();
			String imagePath = utils.getCloudinaryUrlByMultipartImage(multipartFile);
			dto.setProfileImgUrl(imagePath);
			userRepository.updateUser(dto.getUserId(), dto.getUserName(), imagePath);
			return dto;
			
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}


	@Override
	public UserValidationDTO loginUserDto(String email,String password) {
		Utils utils = new Utils();
		ErrorDTO errorDto = new ErrorDTO();
		JwtToken jwtToken = new JwtToken();
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		List<UserDTO> userList = new ArrayList<UserDTO>();
		List<ValidationFactorsDTO> userDtoList = new ArrayList<ValidationFactorsDTO>();
		List<ValidationFactorsDTO> validateUserDtoList = validateUserLoginDto(email,password);
		if(validateUserDtoList.size()>0){
			userValidationDTO.setValidationFactor(validateUserDtoList);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			return userValidationDTO;
		}
		
		if(email != null) email = email.toLowerCase();
		UserDTO userDto =  userRepository.findByEmailAndPassword(email);
		if(!BCrypt.checkpw(password,userDto.getUsrPasswrd())){
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.PASSWORD);
			factorsDTO.setValidationError(Constant.LOGINFAIL);
			userDtoList.add(factorsDTO);
			userValidationDTO.setValidationFactor(validateUserDtoList);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setValidationFactor(userDtoList);
			return userValidationDTO;
		}
		
		userDto.setUsrPasswrd("");
		userList.add(userDto);
		userValidationDTO.setUserDto(userList);
		userValidationDTO.setHttpCode(200);
		userValidationDTO.setStatus(1);
		userValidationDTO.setAccessToken(jwtToken.generateJWTToken(userDto));
		
		return userValidationDTO;
	}

	@Override
	public UserValidationDTO userList() {
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		List<UserDTO> userDtoList = (List<UserDTO>) userRepository.findAll();
		userValidationDTO.setUserDto(userDtoList);
		userValidationDTO.setHttpCode(200);
		userValidationDTO.setStatus(1);
		return userValidationDTO;
	}

	@Override
	public UserValidationDTO getUserByUserName(String userName) {
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		UserDTO userDto = userRepository.findUserByUserName(userName);
		List<ValidationFactorsDTO> validateFactorDto = new ArrayList<ValidationFactorsDTO>();
		List<UserDTO> userDtoList = new ArrayList<>();
		if(userDto != null){
			userDtoList.add(userDto);
			userValidationDTO.setUserDto(userDtoList);
			userValidationDTO.setHttpCode(200);
			userValidationDTO.setStatus(1);
			return userValidationDTO;
		}else{
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.USERNAME);
			factorsDTO.setValidationError(Constant.INVALIDDATA);
			validateFactorDto.add(factorsDTO);
			userValidationDTO.setValidationFactor(validateFactorDto);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setValidationFactor(validateFactorDto);
			return userValidationDTO;
		}
	}

	@Override
	public UserValidationDTO getUserByFirstName(String firstName) {
		UserValidationDTO userValidationDTO = new UserValidationDTO();
		UserDTO userDto = userRepository.findUserByFullName(firstName);
		List<ValidationFactorsDTO> validateFactorDto = new ArrayList<ValidationFactorsDTO>();
		List<UserDTO> userDtoList = new ArrayList<>();
		if(userDto != null){
			userDtoList.add(userDto);
			userValidationDTO.setUserDto(userDtoList);
			userValidationDTO.setHttpCode(200);
			userValidationDTO.setStatus(1);
			return userValidationDTO;
		}else{
			ValidationFactorsDTO factorsDTO = new ValidationFactorsDTO();
			factorsDTO.setFieldName(Constant.USERNAME);
			factorsDTO.setValidationError(Constant.INVALIDDATA);
			validateFactorDto.add(factorsDTO);
			userValidationDTO.setValidationFactor(validateFactorDto);
			userValidationDTO.setStatus(0);
			userValidationDTO.setHttpCode(500);
			userValidationDTO.setValidationFactor(validateFactorDto);
			return userValidationDTO;
		}
	}

	@Override
	public UserValidationDTO followUser(FollowUserDTO followUserDTO) {

		Utils utils = new Utils();
		Integer userId =  (int) context.getAttribute("userId");
		Long id = Long.valueOf(userId);
		
		FollowUserDTO dto = new FollowUserDTO();
		dto.setActor(followUserDTO.getActor());
		dto.setFollower(id);
		dto.setCreateDate(new Date());
		followUserRepository.save(dto);
		return null;
	}
}
