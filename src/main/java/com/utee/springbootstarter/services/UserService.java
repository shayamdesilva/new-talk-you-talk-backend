package com.utee.springbootstarter.services;

import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.FollowUserDetailViewDTO;
import com.utee.springbootstarter.domain.UserValidationDTO;
import com.utee.springbootstarter.repositories.FollowUserDetailsRepository;

public interface UserService {
	
	UserValidationDTO getFollowUsersById(Integer id);
	
	UserValidationDTO getFollowActorById(Integer id);

}
