package com.utee.springbootstarter.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.utee.springbootstarter.domain.FollowUserDTO;
import com.utee.springbootstarter.domain.FollowUserDetailViewDTO;
import com.utee.springbootstarter.domain.UserEmailDTO;
import com.utee.springbootstarter.exceptions.EtAuthException;

public interface FollowUserDetailsRepository extends CrudRepository<FollowUserDetailViewDTO, Integer> {

	@Query(value="SELECT * FROM vm_follow_user_detail u WHERE u.user_id =:userId",nativeQuery = true)
	List<FollowUserDetailViewDTO> findFollowUsersDetailById(@Param("userId") Integer userId) ;
	
	@Query(value="SELECT * FROM vm_follow_user_detail u WHERE u.actor_id =:userId",nativeQuery = true)
	List<FollowUserDetailViewDTO> findActorsDetailById(@Param("userId") Integer userId) ;
	
}
