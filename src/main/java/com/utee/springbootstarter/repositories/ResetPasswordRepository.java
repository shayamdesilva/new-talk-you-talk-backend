package com.utee.springbootstarter.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.utee.springbootstarter.domain.ResetPassword;
import com.utee.springbootstarter.domain.UserDTO;

public interface ResetPasswordRepository extends CrudRepository<ResetPassword, Integer> {
	
	@Query(value="SELECT * FROM utee20_tut_reset_password_token u WHERE u.EMAIL = :email",nativeQuery = true)
	ResetPassword findByEmail(@Param("email") String email);

}
