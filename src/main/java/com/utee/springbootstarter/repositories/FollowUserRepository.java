package com.utee.springbootstarter.repositories;

import org.springframework.data.repository.CrudRepository;

import com.utee.springbootstarter.domain.FollowUserDTO;

public interface FollowUserRepository extends CrudRepository<FollowUserDTO, Long>{

}
