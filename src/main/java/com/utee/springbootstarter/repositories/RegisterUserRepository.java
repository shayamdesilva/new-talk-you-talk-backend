package com.utee.springbootstarter.repositories;

import java.util.List;


import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.exceptions.EtAuthException;

public interface RegisterUserRepository extends CrudRepository<UserDTO, Integer> {
	
	@Query(value="SELECT * FROM utee20_tut_user u WHERE u.email = :email",nativeQuery = true)
	UserDTO findByEmailAndPassword(@Param("email") String email) throws EtAuthException;
	
	@Query(value="SELECT COUNT(*) FROM utee20_tut_user u WHERE u.EMAIL = :email",nativeQuery = true)
	Integer getCountByEmail(@Param("email") String email);
	
	@Query(value="SELECT * FROM utee20_tut_user u WHERE u.EMAIL = :email",nativeQuery = true)
	UserDTO findByEmail(@Param("email") String email);


    @Transactional
    @Modifying
	@Query(value="update utee20_tut_user u set u.username =:userName, u.profile_img_url =:imagePath WHERE u.user_id =:userId",nativeQuery = true)
	void updateUser(@Param("userId") Integer userId,@Param("userName") String userName,@Param("imagePath") String imagePath) ;
	
    @Transactional
    @Modifying
	@Query(value="update utee20_tut_user u set u.is_email_verified =:isEmailVerified WHERE u.user_id =:userId",nativeQuery = true)
	int updateUserEmailVerificationByUserid(@Param("userId") Integer userId,@Param("isEmailVerified") Boolean isEmailVerified) ;
	
    @Transactional
    @Modifying
	@Query(value="update utee20_tut_user u set u.user_email_id =:resetPassword WHERE u.email =:email",nativeQuery = true)
	void updateResetPasswordField(@Param("email") String email,@Param("resetPassword") String resetPassword) ;
	
    
//	UserDTO findByUserId(Integer userId);
	
	@Query(value="SELECT * FROM utee20_tut_user u WHERE u.username = :userName",nativeQuery = true)
	UserDTO findUserByUserName(@Param("userName") String userName) ;

	@Query(value="SELECT * FROM utee20_tut_user u WHERE u.firstname = :firstName",nativeQuery = true)
	UserDTO findUserByFullName(@Param("firstName") String firstName) ;
}
