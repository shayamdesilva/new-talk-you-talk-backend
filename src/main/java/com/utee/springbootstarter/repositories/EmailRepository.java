package com.utee.springbootstarter.repositories;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.utee.springbootstarter.domain.UserDTO;
import com.utee.springbootstarter.domain.UserEmailDTO;
import com.utee.springbootstarter.exceptions.EtAuthException;

public interface EmailRepository  extends CrudRepository<UserEmailDTO,Integer>{
	
	 @Transactional
	    @Modifying
		@Query(value="update utee20_tut_useremail u set u.verify_date =:verifyDate , u.is_email_verify=:isEmailVerify WHERE u.email_id =:emailId",nativeQuery = true)
		int updateUserEmailByEmaiID(@Param("emailId") Integer emailId,@Param("isEmailVerify") Boolean isEmailVerify,@Param("verifyDate") Date verifyDate) ;
		

		@Query(value="SELECT * FROM utee20_tut_useremail u WHERE u.user_id =:userId and u.email=:email",nativeQuery = true)
		UserEmailDTO findEmailVerificationKeyByUserId(@Param("userId") Integer userId,@Param("email") String email) throws EtAuthException;
		
		@Query(value="SELECT * FROM utee20_tut_useremail u WHERE email_key=:emailKey",nativeQuery = true)
		UserEmailDTO findEmailVerificationDtoByEmailKey(@Param("emailKey") String emailKey) ;
		
		@Query(value="SELECT * FROM utee20_tut_useremail u WHERE u.email=:email",nativeQuery = true)
		UserEmailDTO isValidEmailByEmailAddress(@Param("email") String email) ;
		
}
