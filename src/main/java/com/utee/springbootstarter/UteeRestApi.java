package com.utee.springbootstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.codec.CodecConfigurer;

import com.utee.springbootstarter.filters.AuthFilter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class UteeRestApi extends SpringBootServletInitializer{


	public static void main(String[] args)  {
		SpringApplication.run(UteeRestApi.class, args);

	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		return builder.sources(UteeRestApi.class);
	}
	
	@Bean
	public FilterRegistrationBean<AuthFilter> filterRegistationBean(){
		FilterRegistrationBean<AuthFilter> filterRegistrationBean = new FilterRegistrationBean<>();
		AuthFilter authFilter = new AuthFilter();
		filterRegistrationBean.setFilter(authFilter);
		filterRegistrationBean.addUrlPatterns("/api/categories/*");
		filterRegistrationBean.addUrlPatterns("/api/profile/*");
//		filterRegistrationBean.addUrlPatterns("/api/users/*");
		return filterRegistrationBean;
		
	}

}




